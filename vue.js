
new Vue({
    el: '#app',
    data: {
        modalTitle: null,
        allCars: [],
        carSchema: {
            _id: null,
            CarName: null,
            CarOwner: null,
            LcPlt: null,
            LotNr: null
        },
        car: {
            id: null
        },
        loginForm: {
            username: null,
            password: null
        },
        loggedIn: false
    },
    mounted: function () {
        this.checkLoggedIn,
        this.getAllCars()
    },
    methods: {
        openModal: function (title, carId) {  
            if (title == 'Update car') {
                this.carSchema = this.allCars.filter(function (carSchema) {
                    return carSchema._id == carId;
                })[0];
    
                this.modalTitle = title + ' ' + this.carSchema.CarName;
    
                this.carBackup = JSON.stringify(this.carSchema);
            } else if (title == 'Add car') {
                this.modalTitle = title;
                this.carSchema = {
                    "CarName": "",
                    "CarOwner": "",
                    "LcPlt": "",
                    "LotNr": ""
                }
            }
        },
        getAllCars() {
            axios.get('http://localhost:3000/cars')
                .then(response => {
                    this.allCars = response.data
                })
        },
        addCar() {
            axios.post('http://localhost:3000/cars', this.carSchema)
                .then(response => {
                    this.car._id = response.data.newCarId;
                    this.allCars.push(this.carSchema)
                })
        },
        removeCarListing(id) {
            this.allCars = this.allCars.filter(function (allCars) {
                return allCars._id != id
            })
        },
        deleteCar(id) {
            axios.delete('http://localhost:3000/cars/' + id)
                .then(response => {
                    this.removeCarListing(id)
                })
        },
        updateCar: function() {
            axios.patch('http://localhost:3000/cars/' + this.carSchema._id, this.carSchema)
            .then(response => {
                if (response.status !== 200) {
                    this.currentCar = this.carSchema
                }
            })
        },
        login() {
            axios.post('http://localhost:3000/login', this.loginForm)
                .then(response => {
                    console.log(response)
                    if(response.status == 200) {
                        this.loggedIn = true
                        console.log('logged in')
                    }
                })
        },
        checkLoggedIn() {
            if(this.getCookie("token")) {
                this.loggedIn = true
            }
        },
        getCookie (name) {  /// Retrieve cookie function
            match = document.cookie.match("token=123");
            if (match) return match[1];
            else return
          },
    }});