'use strict';
var mongoose = require('mongoose'),
Car = mongoose.model('Car');

exports.list_all_cars = function(req, res) {
    Car.find({}, function(err, burger) {
        if (err)
        res.send(err);
        res.json(burger)
    });
};

exports.add_a_car= function(req, res) {
    var new_car = new Car(req.body);
    console.log(req)
    new_car.save(function(err, car) {
        if (err)
        res.send(err);
        res.json({
            newCarId: car._id
        });
    });
};

exports.list_a_car = function(req, res) {
    Car.findById(req.params.id, function(err, car) {
        if (err)
        res.send(err);
        res.json(car);
    });
};

exports.update_a_car = function(req, res) {
    Car.findOneAndUpdate({_id: req.params.id}, req.body, {new: true}, function(err, car) {
      if (err)
        res.send(err);
      res.json(car);
    });
  };

exports.delete_a_car = function(req, res) {
    Car.remove({
        _id: req.params.id
    }, function(err, car) {
        if (err)
        res.send(err);
        res.json({ message: 'Auto on ' });
    });
};

function checkToken (req) {
    if(req.headers.cookie == 'token=123') {
        return true
    }
    return false
}