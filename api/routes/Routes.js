'use strict';
var x = require('../controllers/Controller');
var y = require('../controllers/loginController')


module.exports = function(app) {

    //Routes
    app.route('/cars')
    .get(x.list_all_cars)
    .post(x.add_a_car);

    app.route('/cars/:id')
    .get(x.list_a_car)
    .patch(x.update_a_car)
    .delete(x.delete_a_car);
    
    app.route('/login')
    .post(y.login);
};