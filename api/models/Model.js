'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CarSchema = new Schema({
    CarName: String,
    CarOwner: String,
    LcPlt: String,
    LotNr: Number
},
{
    versionKey: false
}
);

module.exports = mongoose.model('Car', CarSchema);